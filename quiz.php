<!doctype html>
<html lang="it">
<head>
	<meta charset="UTF-8">
	<title>Che tipo di informatico sei?</title>
	<link rel="stylesheet" href="quiz.css">
</head>
<body>
	<h1>Che tipo di informatico sei?</h1>
<?php
	$risultato = array(

		array( 
			'Noob',
			'Che peccato...Ti consiglierei questo libro da leggere, il mio criceto è piu intelligente.',
			'noob_1.jpg',
			'Un consiglio: non toccare il computer prima di causare un blackout mondiale.',
			'noob_2.jpg'
		),

		array(

			'Softcore user',
			'Provi a combinare qualcosa, ma spesso fallisci...Ah, e non mangiare legno davanti al PC, perche riempi la tastiera di segatura.',
			'soft_1.jpg',
			'Te la cavi abbastanza, ma meglio programmare che giocare...Il sonno lascialo da parte, non ti porterà di certo a penetrare i server della NSA',
			'soft_2.jpg'

			),

		array(

			'Nerd/2',
			'Buone conoscenze, ma attenzione a non trascurare troppo la vita sociale. Lavarsi è allo stesso tempo una cosa fondamentale.',
			'nerd_1.jpg',
			'Bravo, vedo che ti impegni. Aiuta anche tua madre a pulire casa ogni tanto, e smettila di fumare...La sigaretta fa male al PC.',
			'nerd_2.jpg'

			),

		array(

			'Hacker++',
			'Ottimo. Eccellente. Niente da dire. Il mondo non è fatto solo di codice.',
			'hacker_1.jpg',
			'hacker_2.jpg'

			)
	);

	if (isset($_POST['quizFinito'])){
			$somma = 0;
			foreach ($_POST as $key => $value) {
				$somma += $value;
			}
			$livello = 0;
			switch ($somma) {
				case ($somma <=10):
					$livello = 0;
					break;
				case ($somma <=20):
					$livello = 1;
					break;
				case ($somma <=25):
					$livello = 2;
					break;
				default:
					$livello = 3;
					break;
			}
			$l = $risultato[$livello];
			?>
			<?php

				if (rand(0,1)){
					$desc = $l[1];//parte cambiata
					$img = $l[2];
				}
				else{
					$desc = $l[3];
					$img = $l[4];
				}

			?>
				<div class="risultato">
					<h1><?php echo $l[0] ?></h1>
					<p><?php echo $desc ?></p>
					<img src="img/<?php echo $img ?>" alt=""/>
				</div>
			<?php

			// 0-10 11-20 21-25 26-30
	} else {
		$domande = ["1) Che sistema operativo utilizzi?","2) Quali linguaggi di programmazione preferisci?",
				"3) Qual'è la risposta al senso della vita?", "4) Quale browser preferisci?", 
				"5) Quale motore di ricerca preferisci?", "6) Cos'è Google per te?", "7) Se dovessi dare un valore a \"10\", quale sarebbe?",
				"8) Qual'è il significato dell'acronimo PHP?", "9) Cosa significa KISS?", "10) Quanto è stato difficile questo questionario?"];
		$risposte = [
		[							// 1
		"Microfrost Finestre" => 1,
		"Mac OS" => 2,
		"Debian" => 2,
		"Nessuno" => 0,
		"Steam OS" => 3
		],
		[							//2
		"Brainfuck" => 3,
		"C++" => 2,
		"Go" => 3,
		"CSS" => 0,
		"Visual Basic" => 1
		],
		[							//3
		"101010" => 3,
		"1" => 1,
		"<3" => 1,
		"1001" => 1,
		"Che ne so" => 0
		],
		[							//4
		"IE" => 1,
		"Firefox" => 2,
		"W3M" => 3,
		"Opera" => 2,
		"Chrome" => 2
		],
		[							//5
		"Google" => 3,
		"Feisbuc chat" => 0,
		"Stack Overflow" => 0,
		"Bing" => 1,
		"Yahoo" => 2
		],
		[							//6
		"Un motore di ricerca figherrimo" => 3,
		"Preferisco Bing" => 0,
		"Facebook" => 0,
		"Cerco di smettere" => 1,
		"La mia fonte di saggezza" => 3
		],
		[							//7
		"10" => 1,
		"2" => 2,
		"8" => 3,
		"A" => 3,
		"3" => 0
		],
		[							//8
		"Personal Home Page" => 2,
		"PHP Hypertext Preprocessor" => 3,
		"PHP?" => 0,
		"People Hurting People" => 0,
		"Particularly Hard (to) Program" => 0
		],
		[							//9
		"Bacio" => 1,
		"Keep It Simple Stupid!" => 3,
		"Una band americana" => 1,
		"Linguaggio di programmazione a basso livello" => 3,
		"La Cipolla" => 0
		],
		[							//10
		"N" => 1,
		"Nan" => 3,
		"Molto" => 0,
		"Poco" => 2,
		"Con Google non troppo" => 0
		]
		];
	 	echo "<form method=\"post\" onsubmit='return submitAllowed();'>";
		for ($i=0; $i < count($domande); $i++) {
			echo "<div class='domanda'><h2>$domande[$i]</h2>";
			$j = 0;
			foreach ($risposte[$i] as $key => $value) {
				echo "<input type=\"radio\" name=\"risposta.$i\" value=\"$value\" id=\"risposta$i.$j\"/> <label for=\"risposta$i.$j\">$key</label> <br />";
				$j++;
			}
			echo "</div>";
		}
		echo "<input type=\"hidden\" name='quizFinito' value='0'/>";
		echo "<input type='submit' value='Invia' '/></form>";
	}
?>
<script src="quiz.js"></script>
</body>
</html>